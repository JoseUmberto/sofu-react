import 'bootstrap/dist/css/bootstrap.min.css';
import 'jquery';
import 'popper.js';
import React from 'react';
import './main.scss'
import avatar from './../../assets/images/avatars/avatar-1.png'

import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStroopwafel } from '@fortawesome/free-solid-svg-icons'
import { faBars } from '@fortawesome/free-solid-svg-icons'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import { faAngleDown } from '@fortawesome/free-solid-svg-icons'

library.add(faStroopwafel, faBars, faSearch, faAngleDown)

//deve-se criar um selector aqui parecido com app-main-page

export default class Main extends React.Component {

  constructor(props){
    super(props)
    this.onResize();
    this.state = {isSmallMenuMode: false}
    this.state = {isMenuCollapsed: false}
    this.state = {isMenuClosed: this.isSmallWidth()}
    this.state = {isOverlayMenuMode: this.isSmallWidth()}
    this.state = {isFixedHeader: true}
    this.state = {isBoxedLayout: false}

    this.toggle = this.toggle.bind(this);
    this.menuClosed = this.menuClosed.bind(this);

    this.state = {
      dropdownOpen: false
    };

  }
  
  onResize() {
    if (this.isSmallWidth()) {
      console.log("AEEEE")
      this.isOverlayMenuMode = true;
      this.isMenuClosed = true;
    }
  }  

  toggle(){
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  }

  menuClosed(){
    this.setState({
      isMenuClosed: !this.isSmallWidth()
    });
    console.log(this.state.isMenuClosed)
  }

  isSmallWidth() {
    return window.innerWidth < 700;
  }

  render(){
    return (
      <div className={`application ${this.state.isMenuClosed ? 'closed-nav': ''}
       ${this.state.isOverlayMenuMode ? 'overlay-nav': ''}
       ${this.state.isMenuCollapsed ? 'small-nav': ''}
       ${this.state.isSmallMenuMode ? 'small-nav-mode': ''}
       ${this.state.isFixedHeader ? 'fixed-header': ''}
       `}>
        <header className="d-flex justify-content-start align-items-center">
          <div className="menu" onClick={this.menuClosed}>
            <FontAwesomeIcon icon="bars" size="2x" />
          </div>
          <div className="search input-group">
            <input type="text" className="form-control pr-2 py-0" placeholder="Search"></input>
            <div className="input-group-append">
              <i className="material-icons pr-2"><FontAwesomeIcon icon="search" /></i>
            </div>
          </div>
          <div className="ml-auto d-flex justify-content-start align-items-center">
            <img className="user-avatar d-none d-md-block" src={avatar} alt="avatar"></img>
            <div className="user-name d-none d-md-block">John Collins</div>
            <Dropdown  className="dropdown-no-toggle dropdown-bounce" isOpen={this.state.dropdownOpen} toggle={this.toggle} placement="bottom-right">
              <DropdownToggle caret>
                <i className="px-4"><FontAwesomeIcon icon="angle-down"/></i> 
              </DropdownToggle>
              <DropdownMenu className="dropdown-menu">
                <DropdownItem header className="dropdown-header d-flex justify-content-end align-items-center">
                  <img className="user-avatar" src={avatar} alt="avatar"></img>
                  <div className="user-name">John Collins</div>
                </DropdownItem>
                <DropdownItem divider></DropdownItem>
                <DropdownItem>Messages</DropdownItem>
                <DropdownItem>Profile</DropdownItem>
                <DropdownItem>Options</DropdownItem>
              </DropdownMenu>
            </Dropdown>
          </div>
        </header>
        <div className="main-content">
          <div className={`
            ${!this.state.isBoxedLayout ? 'container-fluid': 'container'}
          `}>
          </div>
        </div>
        <div className="side-nav">
          <div className="back-btn">
            <i className="px-4"><FontAwesomeIcon icon="angle-down"/></i> 
          </div>
        </div>
      </div>
    )
  }
}